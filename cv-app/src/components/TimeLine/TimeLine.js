import React from 'react';
import TimeLineData from './TimeLineData.js';
import './TimeLine.scss'

function TimeLine() {
    return (
        <div className="timeline-container">
            <h1 className='Title'>Education</h1>
            {TimeLineData.map((event, index, array) => (
                <div key={index} className="timeline-event">
                    <div className="dates-container">
                        <div className="event-date">{event.date}</div>
                        {index !== array.length - 1 && <div className="vertical-line"></div>}
                    </div>
                    <div className="content-container">
                        <h3>{event.title}</h3>
                        <p className="text">{event.text}</p>
                    </div>
                </div>
            ))}
        </div>
    );
}

export default TimeLine;