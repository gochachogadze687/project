import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './ScrollToTop.scss';

const ScrollToTop = () => {

    const handleScrollToTop = () => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    };

    return (
        <button className="scrollToTopButton" onClick={handleScrollToTop}>
            <FontAwesomeIcon icon="arrow-up" />
            ∧
        </button>
    );
}

export default ScrollToTop;