import React from 'react';
import './Button.scss';
import { useNavigate } from 'react-router-dom';

function Button({ icon, text, navigateTo }) {
    const navigate = useNavigate();
    
    const handleClick = () => {
        if (navigateTo) {
            navigate(navigateTo);
        }
    }

    return (
        <button className="customButton" onClick={handleClick}>
            {icon && <span className="buttonIcon">{icon}</span>}
            <span className="buttonText">{text}</span>
        </button>
    );
}

export default Button;