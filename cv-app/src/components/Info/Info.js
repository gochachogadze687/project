import React from 'react';
import { Link } from 'react-router-dom';
import './Info.scss';
import backgroundImage from '../../assets/images/background.png';
import userAvatar from '../../assets/images/UserAvatar.png';  

function Info() {
    const style = {
        backgroundImage: `url(${backgroundImage})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        backgroundRepeat: 'no-repeat'
    };

    return (
        <div className="info-container" style={style}>
            <div className="info-overlay"></div>
            <div className="info-content">
                <img src={userAvatar} alt="John Doe" className="info-image" /> 
                <h1 className="info-title">John Doe</h1>
                <p className="info-text">Programmer. Creative. Innovator.</p>
                <p className="info-description">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque</p>
                <Link to="/inner" className="info-button">Know more</Link>
            </div>
        </div>
    );
}

export default Info;