import React from 'react';
import FeedbackItem from './FeedbackItem'; 
import './FeedbackList.scss';
import ellipseImage from '../../assets/images/Ellipse_1.png';



const feedbackData = [
    { 
      feedback: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. ', 
      reporter: { 
        photoUrl: ellipseImage, 
        name: 'Martin Friman Programmer,', 
        citeUrl: 'https://www.somesite.com' 
      } 
    }, 
    { 
      feedback: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. ', 
      reporter: { 
        photoUrl: ellipseImage, 
        name: 'Martin Friman Programmer,', 
        citeUrl: 'https://www.somesite.com' 
      } 
    }
];

const FeedbackList = () => { 
  if (!Array.isArray(feedbackData) || feedbackData.length === 0) {
    return <div>No feedback available.</div>;
  }

  return (
    <div className="feedback-list">
      <h1 className='title'>Feedbacks</h1> 
      {feedbackData.map((item, index) =>  
        <FeedbackItem 
          key={index} 
          feedback={item.feedback} 
          reporter={item.reporter} 
        />
      )}
    </div>
  );
};

export default FeedbackList;