import React from 'react';
import PropTypes from 'prop-types';
import './FeedbackItem.scss';

const FeedbackItem = ({ feedback, reporter }) => (
  <div className="feedback-item">
    <p className='feedbackText'>{feedback}</p>
    <div className="reporter-info">
      <img src={reporter.photoUrl} alt={reporter.name} className="reporter-photo" />
      <span className="reporter-name">{reporter.name}</span>
      <a href={reporter.citeUrl} className="reporter-cite">somesite.com</a>
    </div>
  </div>
);

FeedbackItem.propTypes = {
  feedback: PropTypes.string.isRequired,
  reporter: PropTypes.shape({
    photoUrl: PropTypes.string,
    name: PropTypes.string,
    citeUrl: PropTypes.string
  }).isRequired
};

FeedbackItem.defaultProps = {
  feedback: 'No feedback provided.',
  reporter: {
    photoUrl: './default.jpg',
    name: 'Anonymous',
    citeUrl: '#'
  }
};

export default FeedbackItem;