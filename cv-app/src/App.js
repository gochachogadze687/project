import React, { useState } from 'react';
import './App.scss';
import { Element } from 'react-scroll';
import Box from './components/Box/Box';
import Button from './components/Button/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Expertise from './components/Expertise/Expertise';
import FeedbackList from './components/Feedback/FeedbackList';
import Navigation from './components/Navigation/Navigation';
import Panel from './components/Panel/Panel';
import PhotoBox from './components/PhotoBox/PhotoBox';
import TimeLine from './components/TimeLine/TimeLine';
import Portfolio from './components/Portfolio/Portfolio';
import Address from './components/Address/Address';
import Info from './components/Info/Info'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import ScrollToTop from './components/ScrollToTop/ScrollToTop';

function App() {
  const [showLeftSection, setShowLeftSection] = useState(true);

  return (
    <Router>
      <div className="App">
        <header className="App-header">
          <Routes>
            <Route path="/" element={<Info />} />
            <Route path="/inner" element={
              <>
                {showLeftSection && (
                  <div className="App-left-section">
                  </div>
                )}

                <div 
                  className="click-svg-container" 
                  style={{ left: showLeftSection ? '22%' : '0' }} 
                >
                  <svg className="click-svg" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg" onClick={() => setShowLeftSection(!showLeftSection)}>
                    <path d="M0 0H25C27.7614 0 30 2.23858 30 5V25C30 27.7614 27.7614 30 25 30H0V0Z" fill="#282c34"/>
                    <path d="M9 20H21M9 10H21H9ZM9 15H21H9Z" stroke="white" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"/>
                  </svg>
                </div>

                {showLeftSection && (
                  <div className="App-left-section">
                    <div className='App-LeftSection1'>
                      <div> 
                        <PhotoBox />
                        <Navigation /> 
                        <Panel />
                      </div>
                      <div className='divButton'>
                        <Button icon={<FontAwesomeIcon icon="chevron-left" />} text="< Go back" className='buttonBack' navigateTo="/"/>
                      </div>
                    </div>
                  </div>
                )}

                <div className="App-right-section" style={{ marginLeft: showLeftSection ? '22%' : '2.4vw' }}>
                  <Element name="AboutMe"> <Box /> </Element>
                  <Element name="Education"> <TimeLine /> </Element>
                  <Element name="Experience"> <Expertise /> </Element>
                  <Element name="Portfolio"> <Portfolio /> </Element>
                  <Element name="Contacts"> <Address /> </Element>
                  <Element name="Feedback"> <FeedbackList /> </Element>
                  <ScrollToTop />
                  
                </div>
              </>
            } />
          </Routes>
        </header>
      </div>
    </Router>
  );
};

export default App;